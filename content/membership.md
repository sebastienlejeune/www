---
title: Members
seo_title: "Our Members"
description: "Discover the organisations that support the OSPO Alliance"
keywords: ["OSPO", "OSPO Alliance", "Open Source Program Office"]
layout: "single"
---

Sign our <a href="/docs/statement_of_support.pdf" target="_blank">statement of support</a> then send it to the [team](mailto:ospo-zone-team@framalistes.org).

Don't forget to subscribe to the [mailing list](https://accounts.eclipse.org/mailing-list/ospo.zone) and to introduce yourself to the community.

{{< section-members >}}

