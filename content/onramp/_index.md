---
title: "OSPO OnRamp"
date: 2021-11-21T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

<img src="/images/OspoOnRamp.png" alt="OSPO OnRamp" style="float: right; margin: 30px">

The OSPO OnRamp meeting series provides a low-threshold entry point for organisations that want to exchange and learn about the basics on how to set up an Open Source Program Office and get started into open source.

The 90 minute meeting is planned in a monthly cadence and will mainly consist of two parts.

In the first part an invited presenter will share experiences, lessons learned and other cool stuff. We plan to record this part of the meeting and to upload the recording later at the OSPO OnRamp website for later reference. This way we hope to gather and retain valuable information for the community.

In the second part of the meeting we would like to provide a trustful and protected environment where all participants can openly share and discuss their challenges, problems or other actual topics around establishing Open Source in their respective organisations. This part of the meeting will be held according to the [Chatham House Rules](https://en.wikipedia.org/wiki/Chatham_House_Rule#:~:text=When%20a%20meeting%2C%20or%20part,other%20participant%2C%20may%20be%20revealed.). Therefore there will be no recording of this part of the meeting.

🌐 [Link to meeting - click to join](https://bbb.opencloud.lu/rooms/flo-iof-4xr-orc/join)

## Next meeting(s)

Meeting will be scheduled for every third Friday of the month from 10:30-12:00 CEST.

We provide an [ICS calendar file](/onramp/OSPO-OnRamp.ics) to easily import the meeting dates in any agenda.

* 📅 **Date**: Friday, December 15th, 10:30-12:00 CET \
  🎙️ **Speakers**: John Whelan — Director of OSPO, [Trinity Innovation, Trinity College](http://www.tcd.ie/innovation), University of Dublin. \
  📜 **Agenda**: Case  Studies from the Frontiers of Knowledge Transfer in a European University.

    Trinity is the first University in Europe to have an official OSPO. However for many years we have supported open source in our Technology Transfer Office through
    - Spinning Out Campus Companies with OSS business models.
    - Dual Licensing of Government Funded  Research.
    - Open Source Education and Training for Researchers.
  
    These will be illustrated through practical and real case studies over the years.
----

## Previous meetings

You can find all available recordings and available content from the previous meetings [🔄 here](/onramp/past_meetings).

## Mailing list

All news, infos and updates will be shared on the OSPO OnRamp mailing list.

Please subscribe at <https://framalistes.org/sympa/info/ospo.onramp>.

## Meeting topics

We would love to see topic proposals for the meeting via the maillinglist. The following list can be seen as starting point, which we will extend based on the community input:

* How to select the "right" open source project for adoption and contributions.
* How to identity sustainable, secure open source projects with respect also to processes and documentation.
* How to attract/raise interest of the C-Level management for a dedicated Open Source strategy.
* How to ease the formal "entry barriers" for developers to join open source activities.
* How to engage with the Open Source community as a company.

We don’t plan to establish a formal voting process but confirming interest into proposed projects via the mailing list will help us to identify the most relevant topics for the community.

Initially we will reach out to the OSPO Alliance network to identify speakers, who are willing to share insights from their perspective, but mid- to long-term we hope that the community itself will identify the right speakers for upcoming meetings.

## Additional Resources

Resources coming from different Open Source and/or OSPO communities and Open Source professionals to help people advance in their open source journey.

Guides and books:

* [OSS Good Governance Handbook](https://ospo-alliance.org/ggi/)
* [Open Source Program Offices: The Primer on Organizational Structures, Roles and Responsibilities, and Challenges](https://www.linkedin.com/pulse/open-source-program-offices-primer-organizational-roles-haddad/)
