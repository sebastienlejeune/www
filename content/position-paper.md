---
title: "OSPO Alliance Position Paper"
date: 2021-06-21T10:00:00-04:00
show_featured_footer: false
layout: "single"
---

[PDF version](/docs/ospo-alliance-position-paper.pdf)

This text is the public declaration of intent of the [OSPO Alliance](/), the Alliance promoting OSS Good Governance.


### Executive Summary

- The OSPO Alliance was launched by European non profit organisations and concerned individuals to promote an approach to excellence in open source software management.
- As open source is becoming mainstream a growing proportion of organisations, public and private, need guidance to implement professional management of open source software.
- The OSPO Alliance provides an open experience-sharing platform to facilitate discovery of tools and best practices and help define the state of the art in this domain.
- The OSPO Alliance provides actionable solutions and a comprehensive methodology to help set-up and manage corporate OSPOs.
- The OSPO Alliance shares the values of the open source movement and believes that lowering barriers to open source adoption, contribution, and participation will help disseminate its benefits society-wide.
- The OSPO Alliance is setting up a vendor-neutral experience-sharing platform, the Alliance is open to all willing to sign its Statement of Support, without further cost or commitment.


### Fostering open source excellence

The concept of the Open Source Program Office (OSPO) is currently gaining popularity as a cross-functional team to help define and steer an organisation’s open source management strategy and organizational readiness. The OSPO Alliance is a non-profit initiative launched as an unincorporated association of non-profit open source organisations and individuals with a desire to foster open source excellence in Europe and beyond.

In the US, OSPOs are becoming common in large companies with a technology focus and critical business reliance on open source software. Because the European ecosystem is different both in terms of scale and approach, we believe there is an interest in implementing the concept and intent of an OSPO in this context as well as an opportunity for related European innovation. We expect this approach, accommodating a range of scales of entity and also non-tech-corporate missions, will prove applicable globally.


### Professional management of open source

As documented by several studies, open source software is **becoming mainstream**. For example, Red Hat estimates{{< note-number number="1" >}} that within the next two years, open source will account for two thirds of enterprise software. And a well-known result{{< note-number number="2" >}} published by Sonatype reveals that 80 to 90% of any new application consists of existing components, most of them open source. Now that open source software is everywhere, it cannot be ignored. It must be professionally managed. 	

Open source software is widely used but it is also important for its users to contribute to projects and publish code under an open source license. Open source software encompasses **several dimensions** and has **specific impacts** on technology architecture, software engineering processes, intellectual property, human resources, procurement, etc.

While there may be a growing shared body of experience as to how to manage open source licenses, there is still much to do toward a comprehensive methodology on how to manage open source in all its dimensions. There is no **recognised state of the art** in this domain. This hampers the progress of open source usage -- most organisations move cautiously if they feel they are left on their own.

Helping mature a collective expertise in professional management of open source software by all kinds of organisations in all sectors of activity at the European level will be a boost to the open source ecosystem and a **source of productivity gains and competitiveness** for the whole European economy.


### The OSPO Alliance brings solutions

The OSPO Alliance aims to bring actionable guidance and solutions to all organisations willing to professionally manage the usage, contribution to and publication of open source software, regardless of their size, revenue model or whether public and private.

In particular it will help organisations make the best out of the inexorable advance of open source software in their information systems and processes. The OSPO Alliance will facilitate the discovery and implementation of tools and best practices when engaging with open source software.

By professionalizing the management of open source software, the OSPO Alliance will make engaging with open source software less risky and more predictable. It will lower barriers to adoption of open source and will enable organisations to leverage it to enhance their digital sovereignty.


### The blueprint for establishing OSPOs

The OSPO Alliance is built out of the OSS Good Governance blueprint developed by European open source organisation OW2 to help implement corporate-wide open source policies, and set up OSPOs. The methodology proposes a comprehensive approach based on five objectives inspired by a motivational behaviour model{{< note-number number="3" >}}.

![motivational behaviour model image](/images/position-paper-motivational-behaviour-model.png)

- **Usage Goal**: This Goal is about using OSS and developing related competences while using OSS. It covers technical ability and experience with OSS, plus developing some understanding and awareness of OSS.
- **Trust Goal**: This Goal is about the secure and responsible use of OSS. It covers in particular compliance and dependency management policies. It is about aiming for the state of the art in implementing the right processes.
- **Culture Goal**: This Goal is concerned with developing an OSS culture that will encourage and support best practices. It's about being part of the open source community, sharing experience and being recognized. An individual perspective that contributes at an organisational level.
- **Engagement Goal**: This Goal develops the corporate perspective. Contributing back to open source projects and supporting open source communities. Developing project visibility: communicating and participating in open source industry as well as community events. At this level, the enterprise engages with the OSS ecosystem and contributes to its sustainability.
- **Strategy Goal**: With this Goal the organisation embraces the full potential of OSS. It proactively uses OSS for innovation and competitiveness. It leverages OSS as an enabler of digital transformation and digital sovereignty. C-level open source awareness is achieved.


### An experience-sharing platform to help organisations align with the state of the art

The OSPO Alliance is building a global knowledge-sharing platform at [ospo-alliance.org](https://ospo-alliance.org) and promotes a common approach, the OSS Good Governance methodology, to help organisations share best practices and leverage each other's experience.

Organizations that deploy comprehensive management of open source software will increase their efficiency while reducing risks associated with using, contributing to, and publishing open source software.

Moreover, through mature engagement with open source, organisations will contribute to the growth of the ecosystems that supply their open source software.

The neutral, vendor-independent platform for sharing experience offered by the OSPO Alliance will help define the state of the art in open source management and provide useful guidance.

The OSPO Alliance will help define the role and remit of the Open Source Officer and propose a broadly accepted job description.


### Taking open source values economy-wide

The OSPO Alliance belongs to the open source movement and builds upon its strong individual values of openness and sharing for the common good. We believe we can help disseminate those values society-wide by facilitating broad-scale adoption of open source software.

- We believe growing awareness and excellence in how to properly use, contribute to and publish open source software will improve the competitiveness and enhance the sustainability of the European OSS ecosystem.
- We believe all stakeholders in the OSS ecosystem are interdependent and have a vested interest in the well being and growth of the others.
- We believe there is much more to managing open source software than IP and license compliance.
- We see open source governance as requiring engaging with the whole ecosystem -- supporting local communities, nurturing a healthy relationship with open source software vendors and cultivating service specialists.
- We believe sharing the open source way is the best path to innovation and this is why the OSPO Alliance must be as open as possible.
- We also believe non-profit open source organisations have a duty to help build awareness within large end users (who are also often software developers themselves) and systems integrators and help develop mutually beneficial relationships with the OSS ecosystem.

### How to get in on the action!

The OSPO Alliance is open to all parties willing to learn how to implement good open source governance and to contribute back their experience and expertise.

Note there is no commitment -- financial nor otherwise – associated with supporting and joining the OSPO Alliance.

Please make a stand by signing the [Support Agreement](/docs/statement_of_support.pdf) and join the [mailing list](https://accounts.eclipse.org/mailing-list/plato-dev)!

---
{{< note-number number="1" >}}
{{< note-content url="https://www.redhat.com/cms/managed-files/rh-enterprise-open-source-report-detail-f21756-202002-en.pdf" title="2020 Red Hat Enterprise Open Source Survey" >}}

{{< note-number number="2" >}}
{{< note-content url="https://www.sonatype.com/resources/white-paper-devsecops-community-survey-2018" title="2018 DevSecOps Community Survey Results" >}}

{{< note-number number="3" >}}
{{< note-content content=`Abraham Maslow, 1943 paper "` url="http://psychclassics.yorku.ca/Maslow/motivation.htm" title="A Theory of Human Motivation" other-content=`" in Psychological Review`>}}

{{< note-content content="v1.0, © 2021 OSPO Alliance & authors, licensed under CC-BY-4.0" >}}

Authors: Cédric Thomas (OW2) and Simon Phipps (Meshed Insights Ltd)
